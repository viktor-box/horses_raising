import scrapy
from horse_raising.items import HorseRaisingItem


class HorsesSpider(scrapy.Spider):
    name = 'horses'

    def start_requests(self):
        item = HorseRaisingItem()
        urls = [
            'http://sports.williamhill.com/bet/en-gb/betting/e/11029917/ap/Ascot+-+King+George+VI+Stakes+-+29th+Jul+2017.html',
            'https://www.skybet.com/horse-racing/ascot/event/20693785',
            'http://www.paddypower.com/racing/future-racing/king-george-vi--chase',
        #     TODO - add Webdriver for click on next url
            'https://www.bet365.com/?lng=1&amp;cb=105812028182#/HO/'
        ]
        for url in urls:
            yield scrapy.Request(url=url, meta={'item': item}, callback=self.parse)

    def parse(self, response):
        if response.url == 'http://sports.williamhill.com/bet/en-gb/betting/e/11029917/ap/Ascot+-+King+George+VI+Stakes+-+29th+Jul+2017.html':
            yield self.parse_stake_horses(response)
        elif response.url == 'http://www.paddypower.com/racing/future-racing/king-george-vi--chase':
            yield self.parse_chase_horses(response)

    def parse_chase_horses(self, response):
        item = response.request.meta['item']
        horse_lst = response.css('div.infos.left h4.left::text').extract()
        horses = [str(horse.encode('ascii','ignore').strip('\n\t')) for horse in horse_lst]
        odds = response.css('div.odd a::text').extract()
        odds = map(str, odds)
        item['horse_name'] = horses
        item['odds'] = odds
        item['url'] = response.url
        return item

    def parse_stake_horses(self, response):
        item = response.request.meta['item']
        horse_lst = response.css('td::text').extract()
        horses = [str(horse.strip('\n\t')) for horse in horse_lst if horse.strip('\n\t') != '']
        odds_lst = response.css('a.oddsBtn::text').extract()
        odds = [str(odds.strip('\n\t')) for odds in odds_lst]
        item['horse_name'] = horses
        item['odds'] = odds
        item['url'] = response.url
        return item
