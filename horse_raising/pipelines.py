# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import os
import csv
import pandas as pd


class HorseRaisingPipeline(object):

    def open_spider(self, spider):
        self.collection = []
        self.df = pd.DataFrame()

    def close_spider(self, spider):
        self.df = pd.DataFrame(self.collection)
        res_df = self.transpose_df(self.df)
        print 'Matrix --->\n\n', res_df
        outpath = os.path.join(os.getcwd(), 'output.csv')
        res_df.to_csv(outpath, index=False, quoting=csv.QUOTE_NONNUMERIC)

    def process_item(self, item, spider):
        self.collection.append(dict(item))
        return item

    def transpose_df(self, df):
        horse_series = df['horse_name']
        odds_series = df['odds']
        horses_df = self.get_df(horse_series, 'horses')
        odds_df = self.get_df(odds_series, 'odds')
        res_df = pd.concat([horses_df, odds_df], axis=1)
        return res_df

    @staticmethod
    def get_df(series, column_name):
        data = []
        for _, value in series.iteritems():
            data.extend(value)
        df = pd.DataFrame(data=data, columns=[column_name])
        return df
