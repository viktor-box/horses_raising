#!/bin/bash

# you need add your directory to the project and export python path
# also to be working every 5 mins you need to add the following lines in `crontab -e`
# PATH=/usr/bin
# */5 * * * * cd project_folder/project_name/ && scrapy crawl spider_name

cd /myfolder/horse_raising/
PATH=$PATH:/usr/local/bin
export PATH
scrapy crawl horses
